# OONI-Exporter

OONI-Exporter fetches RiseupVPN OONI test results from AWS, parses and provides them for Prometheus.


## Requirements: 
- install Go 1.15

To fetch data directly from AWS, you have to install the aws client, this is not mandatory: 

```
$ curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
```


## Install
```
go build
```


## Run the client
... during development

help:

```
go run main.go -h
```

this example command will fetch test results between Oct. 30th 2020 and Nov. 2nd 2020

```
go run main.go -s 20201030 -e 20201102
```

It is recommended to use the ooni API instead of AWS, because the structure of the AWS buckets may change whereas the API (hopefully) won't have breaking changes. The API is also faster than AWS queries. If you need to fetch data from AWS (e.g. to store all data locally) add -a

```
go run main.go -a 
```
*Fetches yesterday's today's test results from AWS.*


## Running from Docker 

1. run from project root folder ` docker build .`
2. get docker image id: ` docker images | head -2 | tail -1 | awk '{print $3}' `
3. run docker image (if prometheus-pushgateway runs on hosts localhost) in Linux:
`docker run --rm --network="host" <IMAGEID>`