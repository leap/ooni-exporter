FROM debian:bullseye-backports AS build
RUN apt-get -q update && env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        build-essential pkg-config git ca-certificates \
&& DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends -t bullseye-backports golang-go \
&& apt-get clean && rm -rf /var/lib/apt/lists/*

ENV GOPATH=/go
WORKDIR $GOPATH
RUN git clone https://0xacab.org/leap/ooni-exporter.git /ooni-exporter && cd /ooni-exporter && go build
RUN strip /ooni-exporter/ooni-exporter

# S6 configuration
FROM registry.git.autistici.org/ai3/docker/s6-base
COPY --from=build /ooni-exporter/ooni-exporter /usr/local/bin/ooni-exporter

COPY docker/conf /etc/

#COPY openvpn-metrics.sh /usr/local/sbin/openvpn-metrics.sh
#COPY chaperone.d/ /etc/chaperone.d
#ENTRYPOINT ["/usr/local/bin/chaperone"]
