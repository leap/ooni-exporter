package util

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// GetVersion returns the integer representation of a version code.
// The version code is allowed to consist of one to two digits per major, minor and patch component.
func GetVersion(versionCode string) (int, error) {
	r, error := regexp.Compile(`^[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}$`)
	if error != nil {
		return 0, error
	}
	if !r.MatchString(versionCode) {
		return 0, fmt.Errorf("version code didn't follow expected versioning scheme '00.00.00'")
	}

	versionParts := strings.Split(versionCode, ".")
	patch, e1 := strconv.Atoi(versionParts[2])
	minor, e2 := strconv.Atoi(versionParts[1])
	major, e3 := strconv.Atoi(versionParts[0])

	if e1 != nil || e2 != nil || e3 != nil {
		return 0, fmt.Errorf("Error formatting version code.")
	}
	return patch + minor*100 + major*10000, nil
}
