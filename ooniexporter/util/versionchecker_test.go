package util_test

import (
	"testing"

	"0xacab.org/leap/ooni-exporter/ooniexporter/util"
)

func TestGetVersionOK(t *testing.T) {
	version, error := util.GetVersion("0.2.0")
	if error != nil {
		t.Errorf("failed to parse expexted version code: %v", error)
	}

	if version != 200 {
		t.Errorf("unexpected version code: %d", version)
	}

	version, error = util.GetVersion("0.12.10")
	if error != nil {
		t.Errorf("failed to parse expexted version code: %v", error)
	}

	if version != 1210 {
		t.Errorf("unexpected version code: %d", version)
	}

	version, error = util.GetVersion("99.99.99")
	if error != nil {
		t.Errorf("failed to parse expexted version code: %v", error)
	}

	if version != 999999 {
		t.Errorf("unexpected version code: %d", version)
	}

}

func TestGetVersion_FormattingFailure(t *testing.T) {
	_, error := util.GetVersion("0.0.100")
	if error == nil {
		t.Error("expected error for error code '0.0.100'")
	}

	_, error = util.GetVersion("100.0.0")
	if error == nil {
		t.Error("expected error for error code '100.0.0'")
	}

	_, error = util.GetVersion("0.100.0")
	if error == nil {
		t.Error("expected error for error code '0.100.0'")
	}

	_, error = util.GetVersion("0.1.0.4")
	if error == nil {
		t.Error("expected error for error code '0.1.0.4'")
	}

	_, error = util.GetVersion("0.1")
	if error == nil {
		t.Error("expected error for error code '0.1'")
	}

}
