package model

import (
	"encoding/json"
	"fmt"
	"reflect"

	"0xacab.org/leap/ooni-exporter/ooniexporter/util"
	"github.com/ooni/probe-engine/pkg/experiment/riseupvpn"
	probe "github.com/ooni/probe-engine/pkg/model"
)

// ResultData contains a collection of TestResults
type ResultData struct {
	testResults               map[string]TestResult
	testResultsCount          map[string]int
	countryResults            map[string][]string
	asnResults                map[string][]string
	FailingGatewayConnections map[string][]FailingGatewayConnection // key - gateway IPs
}

// FailingGatewayConnection contains details about transport, port, ip
// of the failing gateway as well as the network and country the test failed in
type FailingGatewayConnection struct {
	GatewayConnection riseupvpn.GatewayConnection
	ProbeASN          string
	ProbeCC           string
	Occurences        int
}

// Equals compares two FailingGatewayConnection by it's struct values exept of the occurences field
func (f *FailingGatewayConnection) Equals(gatewayConnection FailingGatewayConnection) bool {
	return f.GatewayConnection.IP == gatewayConnection.GatewayConnection.IP &&
		f.GatewayConnection.Port == gatewayConnection.GatewayConnection.Port &&
		f.GatewayConnection.TransportType == gatewayConnection.GatewayConnection.TransportType &&
		f.ProbeASN == gatewayConnection.ProbeASN &&
		f.ProbeCC == gatewayConnection.ProbeCC
}

// NewResultData instanciates a new ResultData struct
func NewResultData() *ResultData {
	result := new(ResultData)
	result.testResults = map[string]TestResult{}
	result.testResultsCount = map[string]int{}
	result.countryResults = map[string][]string{}
	result.asnResults = map[string][]string{}
	result.FailingGatewayConnections = map[string][]FailingGatewayConnection{}
	return result
}

// GetTestResultCount returns the sum of registered test results
func (r *ResultData) GetTestResultCount() int {
	counter := 0
	for _, i := range r.testResultsCount {
		counter = counter + i
	}
	return counter
}

// CountryForASN returns the country code for a given network identifier
func (r *ResultData) CountryForASN(asn string) string {
	if testHashes := r.asnResults[asn]; testHashes != nil {
		probeCC := r.testResults[testHashes[0]].ProbeCC
		return probeCC
	}
	return "n/a"
}

// FailingGatewayConnectionsByIPAndCountry returns a map containing IPs as keys and lists
// of failing connections sorted by countries as values
func (r *ResultData) FailingGatewayConnectionsByIPAndCountry() map[string][]FailingGatewayConnection {
	results := map[string][]FailingGatewayConnection{}
	for ip, failingConnections := range r.FailingGatewayConnections {
		countryMap := map[string]FailingGatewayConnection{}
		results[ip] = []FailingGatewayConnection{}
		for _, connection := range failingConnections {
			if _, ok := countryMap[connection.ProbeCC]; !ok {
				connection.GatewayConnection.TransportType = "n/a"
				connection.GatewayConnection.Port = -1
				connection.ProbeASN = "n/a"
				countryMap[connection.ProbeCC] = connection
			} else {
				existingEntry := countryMap[connection.ProbeCC]
				existingEntry.Occurences = existingEntry.Occurences + connection.Occurences
				countryMap[connection.ProbeCC] = existingEntry
			}
		}
		for _, connection := range countryMap {
			results[ip] = append(results[ip], connection)
		}
	}
	return results
}

// FailingGatewayConnectionsByIPAndTransportAndCountry returns a map containing IPs as keys and lists
// of failing connections sorted by countries and transport types as values
func (r *ResultData) FailingGatewayConnectionsByIPAndTransportAndCountry() map[string][]FailingGatewayConnection {
	results := map[string][]FailingGatewayConnection{}
	for ip, failingConnections := range r.FailingGatewayConnections {
		countryTransportMap := map[string]FailingGatewayConnection{}
		results[ip] = []FailingGatewayConnection{}
		for _, connection := range failingConnections {
			key := connection.ProbeCC + connection.GatewayConnection.TransportType
			if _, ok := countryTransportMap[key]; !ok {
				connection.GatewayConnection.Port = -1
				connection.ProbeASN = "n/a"
				countryTransportMap[key] = connection
			} else {
				existingEntry := countryTransportMap[key]
				existingEntry.Occurences = existingEntry.Occurences + connection.Occurences
				countryTransportMap[key] = existingEntry
			}
		}
		for _, connection := range countryTransportMap {
			results[ip] = append(results[ip], connection)
		}
	}
	return results
}

// FailingGatewayConnectionsByIPAndNetwork returns a map containing IPs as keys and lists
// of failing connections sorted by network ASNs as values
func (r *ResultData) FailingGatewayConnectionsByIPAndNetwork() map[string][]FailingGatewayConnection {
	results := map[string][]FailingGatewayConnection{}
	for ip, failingConnections := range r.FailingGatewayConnections {
		networkMap := map[string]FailingGatewayConnection{}
		results[ip] = []FailingGatewayConnection{}
		for _, connection := range failingConnections {
			if _, ok := networkMap[connection.ProbeASN]; !ok {
				connection.GatewayConnection.TransportType = "n/a"
				connection.GatewayConnection.Port = -1
				networkMap[connection.ProbeASN] = connection
			} else {
				existingEntry := networkMap[connection.ProbeASN]
				existingEntry.Occurences = existingEntry.Occurences + connection.Occurences
				networkMap[connection.ProbeASN] = existingEntry
			}
		}
		for _, connection := range networkMap {
			results[ip] = append(results[ip], connection)
		}
	}
	return results
}

// FailingGatewayStatusByNetwork returns a map with a network identifier and the amount tests with at least one blocked Gateway
func (r *ResultData) FailingGatewayStatusByNetwork() map[string]int {
	return r.matchingTestsInMap(r.asnResults, failingGatewayStatus)
}

// SuccessfulGatewayStatusByNetwork returns a map with a network identifier and the amount tests with no blocked Gateways
func (r *ResultData) SuccessfulGatewayStatusByNetwork() map[string]int {
	return r.matchingTestsInMap(r.asnResults, successfulGatewayStatus)
}

// UnknownGatewayStatusByNetwork returns a map with a network identifier and the amount tests with an unknown Gateway status
// The status can be unknown because they won't be tested if the API used to figure out the available gateways is already blocked
func (r *ResultData) UnknownGatewayStatusByNetwork() map[string]int {
	return r.matchingTestsInMap(r.asnResults, unknownGatewayStatus)
}

// FailingAPIByNetwork returns a map with a network identifier and the amount tests with a blocked API
func (r *ResultData) FailingAPIByNetwork() map[string]int {
	return r.matchingTestsInMap(r.asnResults, failingAPI)
}

// SuccessfulAPIByNetwork returns a map with a network identifier and the amount tests with an accessible API
func (r *ResultData) SuccessfulAPIByNetwork() map[string]int {
	return r.matchingTestsInMap(r.asnResults, successfulAPI)
}

// FailingCAByNetwork returns a map with a network identifier and the amount tests with invalid CA's that lead to test failures
func (r *ResultData) FailingCAByNetwork() map[string]int {
	return r.matchingTestsInMap(r.asnResults, failingCA)
}

// FailingTestsByCountry returns a map with country codes and the amount of failing tests
func (r *ResultData) FailingTestsByCountry() map[string]int {
	return r.matchingTestsInMap(r.countryResults, failingTest)
}

// FailingTestsByNetwork returns a map with network codes and the amount of failing tests
func (r *ResultData) FailingTestsByNetwork() map[string]int {
	return r.matchingTestsInMap(r.asnResults, failingTest)
}

// SuccessfulTestsByCountry returns a map with country codes and the amount of successful tests
func (r *ResultData) SuccessfulTestsByCountry() map[string]int {
	return r.matchingTestsInMap(r.countryResults, successfulTest)
}

// SuccessfulTestsByNetwork returns a map with country codes and the amount of successful tests
func (r *ResultData) SuccessfulTestsByNetwork() map[string]int {
	return r.matchingTestsInMap(r.asnResults, successfulTest)
}

// AddMeasurement adds a test result, counts the occurences of the same result
// and saves references to the country and network used
func (r *ResultData) AddMeasurement(measurement probe.Measurement) {
	result, err := parseMeaseurement(measurement)
	if err == nil {
		r.addTestResult(*result)
	}
}

func successfulTest(test TestResult) bool {
	return test.APIStatus && test.GatewayStatus
}

func failingTest(test TestResult) bool {
	return !test.APIStatus || !test.GatewayStatus
}

func failingAPI(test TestResult) bool {
	return !test.APIStatus
}

func failingCA(test TestResult) bool {
	return !test.CACertStatus
}

func successfulAPI(test TestResult) bool {
	return test.APIStatus
}

func failingGatewayStatus(test TestResult) bool {
	return !test.GatewayStatus
}

func successfulGatewayStatus(test TestResult) bool {
	testVersion, err := util.GetVersion(test.TestVersion)
	compareVersion, _ := util.GetVersion("0.2.0")
	if err == nil && testVersion <= compareVersion {
		return test.GatewayStatus && test.APIStatus
	}
	return test.GatewayStatus
}

func unknownGatewayStatus(test TestResult) bool {
	return test.GatewayStatus && !test.APIStatus
}

func (r *ResultData) matchingTestsInMap(bucket map[string][]string, condition func(TestResult) bool) map[string]int {
	matchingTests := map[string]int{}
	for key, testHashes := range bucket {
		counter := 0
		for _, testHash := range testHashes {
			test := r.testResults[testHash]
			if condition(test) {
				counter++
			}
		}
		matchingTests[key] = counter
	}
	return matchingTests
}

func parseMeaseurement(measurement probe.Measurement) (*TestResult, error) {
	tk := getRiseupVPNTestKeys(measurement)
	apifailure, err := getAPIFailure(tk)
	if err != nil {
		return nil, err
	}
	result := TestResult{
		TestVersion:     measurement.TestVersion,
		EngineName:      measurement.Annotations["engine_name"],
		EngineVersion:   measurement.Annotations["engine_version"],
		ProbeCC:         measurement.ProbeCC,
		ProbeASN:        measurement.ProbeASN,
		ResolverASN:     measurement.ResolverASN,
		ReportID:        measurement.ReportID,
		APIStatus:       tk.APIStatus == "ok",
		GatewayStatus:   tk.FailingGateways == nil || len(tk.FailingGateways) == 0,
		APIFailure:      apifailure,
		FailingGateways: tk.FailingGateways,
		CACertStatus:    tk.CACertStatus,
	}
	return &result, nil
}

func getRiseupVPNTestKeys(measurement probe.Measurement) TestKeys {
	testKeysInterface := measurement.TestKeys
	// convert map to json
	jsonString, _ := json.Marshal(testKeysInterface)

	// convert json to struct
	tk := TestKeys{}
	err := json.Unmarshal(jsonString, &tk)
	if err != nil {
		fmt.Printf("error parsing %v", jsonString)
	}
	return tk
}

func getAPIFailure(testkeys TestKeys) (*string, error) {
	if testkeys.APIFailure != nil {
		// since tk.APIFailure can have different types - either a *string or []string -
		// depending on the riseupvpn test version, we need to handle both of them
		switch t := testkeys.APIFailure.(type) {
		// ugly hack, please fix me:
		// for some reason I cannot directly compare t with []string
		case []interface{}:
			returnValue := ""
			for _, failure := range testkeys.APIFailure.([]interface{}) {
				failureType := reflect.TypeOf(failure)
				if fmt.Sprintf("%v", failureType) != "string" {
					return nil, fmt.Errorf("unexpected type %T", failureType)
				}
				returnValue = fmt.Sprintf("%s,%s,", returnValue, failure)
			}
			returnValue = returnValue[1 : len(returnValue)-1]
			return &returnValue, nil
		case string:
			// riseupvpn test version 0.2.0
			returnValue := fmt.Sprintf("%v", testkeys.APIFailure)
			return &returnValue, nil
		default:
			return nil, fmt.Errorf("unexpected type %T", t)
		}
	}
	return nil, nil
}

// AddTestResult adds a test result, counts the occurences of the same result
// and saves references to the country and network used
func (r *ResultData) addTestResult(result TestResult) {
	hash := result.Hash()
	//log.Println("adding result: " + fmt.Sprint(result))
	if r.testResultsCount[hash] == 0 {
		r.testResults[hash] = result
	}
	r.testResultsCount[hash] = r.testResultsCount[hash] + 1
	probeCC := result.ProbeCC
	if _, ok := r.countryResults[probeCC]; !ok {
		//log.Println("creating a new map for " + probeCC)
		r.countryResults[probeCC] = []string{}
	}
	r.countryResults[probeCC] = append(r.countryResults[probeCC], hash)
	network := result.ProbeASN
	if _, ok := r.asnResults[network]; !ok {
		//log.Println("creating a new map for " + network)
		r.asnResults[network] = []string{}
	}
	r.asnResults[network] = append(r.asnResults[network], hash)

	for _, gateway := range result.FailingGateways {
		ip := gateway.IP
		failingGatewayConnection := FailingGatewayConnection{
			GatewayConnection: gateway,
			Occurences:        1,
			ProbeCC:           result.ProbeCC,
			ProbeASN:          result.ProbeASN,
		}

		// create a new array of FailingGatewayConnections if there's not yet a connection stored
		// with same IP
		if _, ok := r.FailingGatewayConnections[ip]; !ok {
			r.FailingGatewayConnections[ip] = []FailingGatewayConnection{}
		}

		// search for entries with same IP, Port, Transport, ASN and CC and
		// increase the occourences counter if an entry is already there
		found := false
		for i, connection := range r.FailingGatewayConnections[ip] {
			if failingGatewayConnection.Equals(connection) {
				found = true
				connection.Occurences = connection.Occurences + 1
				r.FailingGatewayConnections[ip][i] = connection
				break
			}
		}

		// add current FailingGatewayConnection object to the list if there's no one yet for the given ip
		if !found {
			r.FailingGatewayConnections[ip] = append(r.FailingGatewayConnections[ip], failingGatewayConnection)
		}
	}
}
