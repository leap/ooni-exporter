package model

import (
	riseup "github.com/ooni/probe-engine/pkg/experiment/riseupvpn"
	"github.com/ooni/probe-engine/pkg/experiment/urlgetter"
)

// This file contains the required models to parse ooni riseup tests
// These models represent the riseupvpn test version 0.2.0 and 0.3.0

// TestKeys contains riseupvpn test keys for tests of version 0.3.0.
type TestKeys struct {
	urlgetter.TestKeys
	APIFailure      interface{}                `json:"api_failure"`
	APIStatus       string                     `json:"api_status"`
	CACertStatus    bool                       `json:"ca_cert_status"`
	FailingGateways []riseup.GatewayConnection `json:"failing_gateways"`
	TransportStatus map[string]string          `json:"transport_status"`
	TestVersion     string
}
