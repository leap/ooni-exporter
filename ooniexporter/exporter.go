package ooniexporter

import (
	"fmt"
	"log"
	"time"

	"0xacab.org/leap/ooni-exporter/ooniexporter/model"
	"github.com/pborman/getopt/v2"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/push"
)

// options set by CLI
var (
	globalOptions Options
)

// Options contains the options you can set from the CLI.
type Options struct {
	StartDate      string
	EndDate        string
	PushGatewayURL string
	UseAWS         bool
	Max            int
}

var (
	completionTime = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "oonifetch_last_completion_timestamp_seconds",
		Help: "The timestamp of the last completion of the ooni api fetch job, successful or not.",
	})
	successTime = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "oonifetch_last_success_timestamp_seconds",
		Help: "The timestamp of the last successful completion of the ooni api fetch job.",
	})
	duration = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "oonifetch_job_duration_seconds",
		Help: "The duration of the last ooni api fetch job in seconds.",
	})
)

func init() {
	getopt.FlagLong(
		&globalOptions.StartDate, "startDate", 's', "fetch data from given start date, default: yesterday", "20200101",
	)
	getopt.FlagLong(
		&globalOptions.EndDate, "endDate", 'e', "fetch data util given end date, default: today", "20200102",
	)
	getopt.FlagLong(
		&globalOptions.PushGatewayURL, "pushGateway", 'p', "url of push gateway, default: http://localhost:9091", "https://example.org:9091",
	)
	getopt.FlagLong(
		&globalOptions.UseAWS, "useAws", 'a', "use AWS backend instead of ooni API",
	)

	getopt.FlagLong(
		&globalOptions.Max, "max", 'm', "maximal amount of tests to fetch, default: 1000", "1000",
	)
}

func registerNumberOfHTTPErrors(registry *prometheus.Registry, responseErrors *model.ErrorResult) {
	gaugeOpts := prometheus.GaugeOpts{
		Name: "oonifetch_http_error",
		Help: "The total number http status codes per status code, contains a subset of 400ers and 500ers",
	}

	registerPerHTTPCode(registry, gaugeOpts, "400", responseErrors.GetErrorStatus(400))
	registerPerHTTPCode(registry, gaugeOpts, "401", responseErrors.GetErrorStatus(401))
	registerPerHTTPCode(registry, gaugeOpts, "403", responseErrors.GetErrorStatus(403))
	registerPerHTTPCode(registry, gaugeOpts, "404", responseErrors.GetErrorStatus(404))
	registerPerHTTPCode(registry, gaugeOpts, "405", responseErrors.GetErrorStatus(405))
	registerPerHTTPCode(registry, gaugeOpts, "408", responseErrors.GetErrorStatus(408))

	registerPerHTTPCode(registry, gaugeOpts, "500", responseErrors.GetErrorStatus(500))
	registerPerHTTPCode(registry, gaugeOpts, "502", responseErrors.GetErrorStatus(502))
	registerPerHTTPCode(registry, gaugeOpts, "503", responseErrors.GetErrorStatus(503))
	registerPerHTTPCode(registry, gaugeOpts, "504", responseErrors.GetErrorStatus(504))
	registerPerHTTPCode(registry, gaugeOpts, "505", responseErrors.GetErrorStatus(505))
	registerPerHTTPCode(registry, gaugeOpts, "all", responseErrors.GetAllHTTPStatusCodes())
}

func registerNumberOfErrors(registry *prometheus.Registry, responseErrors *model.ErrorResult) {
	gaugeOpts := prometheus.GaugeOpts{
		Name: "oonifetch_error",
		Help: "The total number of errors occured while fetching from the OONI API and parsing the data, not including http status codes",
	}

	opts := gaugeOpts
	gauge := prometheus.NewGauge(opts)
	gauge.Set(float64(responseErrors.GetAllErrors()))
	registry.MustRegister(gauge)
}

func registerNumberOfResults(registry *prometheus.Registry, resultData *model.ResultData) {
	gaugeOpts := prometheus.GaugeOpts{
		Name: "oonifetch_results",
		Help: "The total number of test results fetched from the OONI API",
	}

	opts := gaugeOpts
	gauge := prometheus.NewGauge(opts)
	gauge.Set(float64(resultData.GetTestResultCount()))
	registry.MustRegister(gauge)
}

func registerTestsPerCountry(registry *prometheus.Registry, resultData *model.ResultData) {
	gaugeOpts := prometheus.GaugeOpts{
		Name: "oonifetch_tests_per_country",
		Help: "The total number successful and failing tests per country",
	}

	registerPerCountry(registry, resultData, gaugeOpts, "success", resultData.SuccessfulTestsByCountry)
	registerPerCountry(registry, resultData, gaugeOpts, "failure", resultData.FailingTestsByCountry)
}

func registerTestsPerNetwork(registry *prometheus.Registry, resultData *model.ResultData) {
	gaugeOpts := prometheus.GaugeOpts{
		Name: "oonifetch_tests_per_network",
		Help: "The total number successful and failing tests per network",
	}

	registerPerNetwork(registry, resultData, gaugeOpts, "success", resultData.SuccessfulTestsByNetwork)
	registerPerNetwork(registry, resultData, gaugeOpts, "failure", resultData.FailingTestsByNetwork)
}

func registerAPIResultsPerNetwork(registry *prometheus.Registry, resultData *model.ResultData) {
	gaugeOpts := prometheus.GaugeOpts{
		Name: "oonifetch_api_results_per_network",
		Help: "The total number successful and failing API tests per network",
	}

	registerPerNetwork(registry, resultData, gaugeOpts, "success", resultData.SuccessfulAPIByNetwork)
	registerPerNetwork(registry, resultData, gaugeOpts, "failure", resultData.FailingAPIByNetwork)
	registerPerNetwork(registry, resultData, gaugeOpts, "ca_cert_issues", resultData.FailingCAByNetwork)
}

func registerGatewayStatusPerNetwork(registry *prometheus.Registry, resultData *model.ResultData) {
	gaugeOpts := prometheus.GaugeOpts{
		Name: "oonifetch_gateway_status_per_network",
		Help: "The total number gateways tests with no blocked gateways (success) or at least one blocked gateway (failure) per network",
	}

	registerPerNetwork(registry, resultData, gaugeOpts, "success", resultData.SuccessfulGatewayStatusByNetwork)
	registerPerNetwork(registry, resultData, gaugeOpts, "n/a", resultData.UnknownGatewayStatusByNetwork)
	registerPerNetwork(registry, resultData, gaugeOpts, "failure", resultData.FailingGatewayStatusByNetwork)
}

func registerFailingGatewaysPerConnectionDetail(registry *prometheus.Registry, resultData *model.ResultData) {
	gaugeOpts := prometheus.GaugeOpts{
		Name: "oonifetch_failing_gateway_detail",
		Help: "The total number of blocked gateways connections per country, network, IP, port and transport",
	}

	for _, failingGatewayConnections := range resultData.FailingGatewayConnections {
		for _, connection := range failingGatewayConnections {
			opts := gaugeOpts
			opts.ConstLabels = prometheus.Labels{
				"probeCC":   connection.ProbeCC,
				"network":   connection.ProbeASN,
				"ip":        connection.GatewayConnection.IP,
				"port":      fmt.Sprint(connection.GatewayConnection.Port),
				"transport": connection.GatewayConnection.TransportType,
			}
			gauge := prometheus.NewGauge(opts)
			gauge.Set(float64(connection.Occurences))
			registry.MustRegister(gauge)
		}
	}
}

func registerFailingGatewaysPerIPAndTransportAndCountry(registry *prometheus.Registry, resultData *model.ResultData) {
	gaugeOpts := prometheus.GaugeOpts{
		Name: "oonifetch_failing_gateway_ip_transport_country",
		Help: "The total number of blocked gateway connections per IP, transport and country",
	}

	for _, failingGatewayConnections := range resultData.FailingGatewayConnectionsByIPAndTransportAndCountry() {
		for _, connection := range failingGatewayConnections {
			opts := gaugeOpts
			opts.ConstLabels = prometheus.Labels{
				"probeCC":   connection.ProbeCC,
				"ip":        connection.GatewayConnection.IP,
				"transport": connection.GatewayConnection.TransportType,
			}
			gauge := prometheus.NewGauge(opts)
			gauge.Set(float64(connection.Occurences))
			registry.MustRegister(gauge)
		}
	}
}

func registerFailingGatewaysPerIPAndNetwork(registry *prometheus.Registry, resultData *model.ResultData) {
	gaugeOpts := prometheus.GaugeOpts{
		Name: "oonifetch_failing_gateway_ip_network",
		Help: "The total number of blocked gateway connections per IP and network",
	}

	for _, failingGatewayConnections := range resultData.FailingGatewayConnectionsByIPAndNetwork() {
		for _, connection := range failingGatewayConnections {
			opts := gaugeOpts
			opts.ConstLabels = prometheus.Labels{
				"probeCC": connection.ProbeCC,
				"network": connection.ProbeASN,
				"ip":      connection.GatewayConnection.IP,
			}
			gauge := prometheus.NewGauge(opts)
			gauge.Set(float64(connection.Occurences))
			registry.MustRegister(gauge)
		}
	}
}

func registerFailingGatewaysPerIPAndCountry(registry *prometheus.Registry, resultData *model.ResultData) {
	gaugeOpts := prometheus.GaugeOpts{
		Name: "oonifetch_failing_gateway_ip_country",
		Help: "The total number of blocked gateway connections per IP and country",
	}

	for _, failingGatewayConnections := range resultData.FailingGatewayConnectionsByIPAndCountry() {
		for _, connection := range failingGatewayConnections {
			opts := gaugeOpts
			opts.ConstLabels = prometheus.Labels{
				"probeCC": connection.ProbeCC,
				"ip":      connection.GatewayConnection.IP,
			}
			gauge := prometheus.NewGauge(opts)
			gauge.Set(float64(connection.Occurences))
			registry.MustRegister(gauge)
		}
	}
}

func registerPerHTTPCode(registry *prometheus.Registry, gaugeOpts prometheus.GaugeOpts, httpCodeLabel string, count int) {
	opts := gaugeOpts
	opts.ConstLabels = prometheus.Labels{
		"httpResponseCode": httpCodeLabel,
	}
	gauge := prometheus.NewGauge(opts)
	gauge.Set(float64(count))
	registry.MustRegister(gauge)
}

func registerPerNetwork(registry *prometheus.Registry, resultData *model.ResultData, gaugeOpts prometheus.GaugeOpts, statusLabel string, networkMap func() map[string]int) {
	for network, count := range networkMap() {
		opts := gaugeOpts
		country := resultData.CountryForASN(network)
		opts.ConstLabels = prometheus.Labels{
			"probeCC": country,
			"network": network,
			"status":  statusLabel,
		}
		gauge := prometheus.NewGauge(opts)
		gauge.Set(float64(count))
		registry.MustRegister(gauge)
	}
}

func registerPerCountry(registry *prometheus.Registry, resultData *model.ResultData, gaugeOpts prometheus.GaugeOpts, statusLabel string, countryMap func() map[string]int) {
	for country, count := range countryMap() {
		opts := gaugeOpts
		opts.ConstLabels = prometheus.Labels{
			"probeCC": country,
			"status":  statusLabel,
		}
		gauge := prometheus.NewGauge(opts)
		gauge.Set(float64(count))
		registry.MustRegister(gauge)
	}
}

// Main is the main function of oonifetch. This function parses the command line
// options and uses a global state.
// This function will panic in case of a fatal error. It is up to you that
// integrate this function to either handle the panic of ignore it.
func Main() {

	getopt.Parse()
	startDate, err := getStartDate()
	if err != nil {
		return
	}

	endDate, err := getEndDate()
	if err != nil {
		return
	}

	log.Println("enddate: " + fmt.Sprint(endDate))
	log.Println("startdate: " + fmt.Sprint(startDate))
	log.Println("useAWS: " + fmt.Sprint(globalOptions.UseAWS))
	log.Println("max tests: " + fmt.Sprint(globalOptions.Max))

	// We use a registry here to benefit from the consistency checks that
	// happen during registration.
	registry := prometheus.NewRegistry()
	pusher := push.New(getPushGateway(), "ooni_fetch").Gatherer(registry)

	// Note that successTime is not registered.
	registry.MustRegister(completionTime, duration)
	start := time.Now()

	var resultData *model.ResultData
	var responseErrors *model.ErrorResult
	if globalOptions.UseAWS {
		resultData, responseErrors = FetchDataFromAWS(*startDate, *endDate, getMaxFetches())
	} else {
		resultData, responseErrors = FetchDataFromAPI(*startDate, *endDate, getMaxFetches())
	}

	registerNumberOfHTTPErrors(registry, responseErrors)
	registerNumberOfErrors(registry, responseErrors)
	registerNumberOfResults(registry, resultData)
	registerTestsPerCountry(registry, resultData)
	registerTestsPerNetwork(registry, resultData)
	registerAPIResultsPerNetwork(registry, resultData)
	registerGatewayStatusPerNetwork(registry, resultData)
	registerFailingGatewaysPerConnectionDetail(registry, resultData)
	registerFailingGatewaysPerIPAndTransportAndCountry(registry, resultData)
	registerFailingGatewaysPerIPAndNetwork(registry, resultData)
	registerFailingGatewaysPerIPAndCountry(registry, resultData)
	// Note that time.Since only uses a monotonic clock in Go1.9+.
	duration.Set(time.Since(start).Seconds())
	completionTime.SetToCurrentTime()
	if err != nil {
		log.Println("fetching ooni data failed:", err)
	} else {
		// Add successTime to pusher only in case of success.
		// We could as well register it with the registry.
		// This example, however, demonstrates that you can
		// mix Gatherers and Collectors when handling a Pusher.
		pusher.Collector(successTime)
		successTime.SetToCurrentTime()
	}
	// Add is used here rather than Push to not delete a previously pushed
	// success timestamp in case of a failure.
	if err := pusher.Add(); err != nil {
		log.Println("Could not push to Pushgateway:", err)
	} else {
		log.Println("Done! Successfully pushed tests to push gateway.")
	}
}

// getStartDate parses the global options if available or uses yesterday's time at 00:00h
func getStartDate() (*time.Time, error) {
	if globalOptions.StartDate == "" {
		startTime := time.Now().AddDate(0, 0, -1)
		// set start date to 00:00h
		startTime = time.Date(startTime.Year(), startTime.Month(), startTime.Day(), 0, 0, 0, 0, startTime.Location())
		return &startTime, nil
	}
	startTime, err := time.ParseInLocation("20060102", globalOptions.StartDate, time.Now().Location())
	if err != nil {
		log.Fatal("wrong formatting for start date. Expecting: YYYYMMDD")
		return nil, err
	}
	return &startTime, nil
}

// getStartDate parses the global options if available or uses today time at 00:00h
func getEndDate() (*time.Time, error) {
	// get end date
	if globalOptions.EndDate == "" {
		endTime := time.Now()
		// set end date to 00:00h
		endTime = time.Date(endTime.Year(), endTime.Month(), endTime.Day(), 0, 0, 0, 0, endTime.Location())
		return &endTime, nil
	}
	endTime, err := time.ParseInLocation("20060102", globalOptions.EndDate, time.Now().Location())
	if err != nil {
		log.Fatal("wrong formatting for end date. Expecting: YYYYMMDD")
		return nil, err
	}

	return &endTime, nil
}

func getMaxFetches() int {
	if globalOptions.Max == 0 {
		return 1000
	}
	return globalOptions.Max
}

func getPushGateway() string {
	if globalOptions.PushGatewayURL == "" {
		return "http://localhost:9091"
	}
	return globalOptions.PushGatewayURL
}
