package ooniexporter

import (
	"compress/gzip"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"0xacab.org/leap/ooni-exporter/ooniexporter/model"
	"github.com/ooni/probe-engine/pkg/experiment/riseupvpn"
	probe "github.com/ooni/probe-engine/pkg/model"
)

// HTTPClient interface declared to be able to mock responses
type HTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
}

var (
	// Client global instance of HTTPClient interface
	Client HTTPClient
)

func init() {
	Client = http.DefaultClient
}

// FetchDataFromAWS fetches RiseupVPN test results from AWS
// It returns a list of dates (which are also the the download root folders)
// where data has been downloaded or an error
func FetchDataFromAWS(start time.Time, end time.Time, max int) (*model.ResultData, *model.ErrorResult) {
	dates, responseErr := fetchFromAWS(start, end)
	errorResult := model.NewErrorResult()
	if responseErr != nil {
		errorResult.AddResponseError(responseErr)
		return nil, errorResult
	}
	resultData := scrapeData(dates, max)
	return resultData, errorResult
}

// ScrapeData parses all downloaded test results
func scrapeData(dates []string, max int) *model.ResultData {
	resultData := model.NewResultData()
	fileLocations := getFileLocations(dates)
	for i := 0; i < min(len(fileLocations), max); i++ {
		measurements, err := unzipMeasurements(fileLocations[i])

		if err != nil {
			log.Fatal(err)
			continue
		}

		for _, measurement := range measurements {
			resultData.AddMeasurement(measurement)
		}
	}
	return resultData
}

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

func getRiseupVPNTestKeys(measurement probe.Measurement) riseupvpn.TestKeys {
	testKeysInterface := measurement.TestKeys
	// convert map to json
	jsonString, _ := json.Marshal(testKeysInterface)
	// convert json to struct
	tk := riseupvpn.TestKeys{}
	json.Unmarshal(jsonString, &tk)
	return tk
}

func unzipMeasurements(file string) ([]probe.Measurement, error) {
	var result = []probe.Measurement{}
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	gReader, err := gzip.NewReader(f)
	if err != nil {
		return nil, err
	}
	defer gReader.Close()

	decoder := json.NewDecoder(gReader)
	for {
		var measurement *probe.Measurement
		err := decoder.Decode(&measurement)
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("Error decoding: %v", err)
			//return nil, err
			continue
		}

		result = append(result, *measurement)
	}

	return result, nil
}

func getFileLocations(dates []string) []string {
	paths := []string{}
	for _, date := range dates {
		err := filepath.Walk(date,
			func(path string, info os.FileInfo, err error) error {
				if err != nil {
					return err
				}
				if strings.Contains(info.Name(), ".jsonl.gz") {
					paths = append(paths, path)
				}
				return nil
			})
		if err != nil {
			log.Println("could not find results for: " + fmt.Sprint(err))
		}
	}
	log.Println("found paths " + fmt.Sprint(paths))
	return paths
}

func fetchFromAWS(start time.Time, end time.Time) ([]string, *model.ResponseError) {
	var dates []string

	// calculate diff in days
	diff := end.Sub(start)
	days := int(diff.Hours() / 24)

	// include all dates to aws command filter
	args := []string{"s3", "--no-sign-request", "sync", "s3://ooni-data-eu-fra/raw/", ".", "--exclude", "*"}
	for i := 0; i <= days; i++ {
		dates = append(dates, start.AddDate(0, 0, i).Format("20060102"))
		args = append(args, "--include")
		args = append(args, "*"+dates[i]+"/??/??/riseupvpn*")
		args = append(args, "--include")
		args = append(args, "*"+dates[i]+"/?/??/riseupvpn*")
	}

	//execute
	cmd := exec.Command("aws", args...)
	log.Println(cmd.String())
	log.Println("This may take a while... please wait")

	_, err := cmd.Output()
	if err != nil {
		log.Fatal("Fetching data from AWS failed: " + fmt.Sprint(err))
		responseErr := model.ResponseError{Err: err}
		return nil, &responseErr
	}

	return dates, nil
}

// FetchDataFromAPI fetches Riseupvpn tests from the OONI API,
// parses the results and returns them as ResultData
func FetchDataFromAPI(start time.Time, end time.Time, max int) (*model.ResultData, *model.ErrorResult) {
	errors := model.NewErrorResult()
	resultData := model.NewResultData()
	log.Println("fetching summary...")

	measurements, responseErr := FetchSummary(Client, start, end)
	if responseErr != nil {
		errors.AddResponseError(responseErr)
		return nil, errors
	}

	for i := 0; i < min(len(measurements.Results), max); i++ {
		log.Println("fetching result no " + fmt.Sprint(i) + "/" + fmt.Sprint(len(measurements.Results)))
		result := measurements.Results[i]
		measurement, responseErr := FetchMeasurement(Client, result)
		if responseErr != nil {
			errors.AddResponseError(responseErr)
			log.Println("Could not fetch measurement: " + fmt.Sprint(responseErr))
			continue
		}
		resultData.AddMeasurement(*measurement)
	}

	return resultData, errors
}

// FetchMeasurement fetches a single OONI Test result
func FetchMeasurement(client HTTPClient, result model.Result) (*probe.Measurement, *model.ResponseError) {
	req, err := http.NewRequest("GET", result.MeasurementURL, nil)
	if err != nil {
		responseError := model.ResponseError{Err: err}
		return nil, &responseError
	}
	req.Header.Add("accept", "application/json")

	response, err := client.Do(req)
	if err != nil {
		responseError := model.ResponseError{Err: err}
		return nil, &responseError
	}

	if response.StatusCode >= 300 {
		statusCode := response.StatusCode
		responseError := model.ResponseError{HTTPStatus: &statusCode}
		return nil, &responseError
	}

	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		responseError := model.ResponseError{Err: err}
		return nil, &responseError
	}

	var measurement probe.Measurement
	if err := json.Unmarshal(body, &measurement); err != nil {
		responseError := model.ResponseError{Err: err}
		return nil, &responseError
	}

	return &measurement, nil
}

// FetchSummary fetches the OONI RiseupVPN network measurement summary
func FetchSummary(client HTTPClient, start time.Time, end time.Time) (*model.Measurements, *model.ResponseError) {
	req, err := http.NewRequest("GET", "https://api.ooni.io/api/v1/measurements", nil)
	if err != nil {
		responseError := model.ResponseError{Err: err}
		return nil, &responseError
	}

	query := req.URL.Query()
	query.Add("test_name", "riseupvpn")
	query.Add("since", start.Format("2006-01-02T00:00:00"))
	query.Add("until", end.Format("2006-01-02T00:00:00"))
	query.Add("limit", "1000")
	req.URL.RawQuery = query.Encode()
	req.Header.Add("accept", "application/json")
	fmt.Println(req.URL.String())

	response, err := client.Do(req)
	if err != nil {
		responseError := model.ResponseError{Err: err}
		return nil, &responseError
	}

	if response.StatusCode >= 300 {
		statusCode := response.StatusCode
		responseError := model.ResponseError{HTTPStatus: &statusCode}
		return nil, &responseError
	}

	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		responseError := model.ResponseError{Err: err}
		return nil, &responseError
	}

	var measurements model.Measurements
	if err := json.Unmarshal(body, &measurements); err != nil {
		responseError := model.ResponseError{Err: err}
		return nil, &responseError
	}
	return &measurements, nil
}
