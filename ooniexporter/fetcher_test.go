package ooniexporter_test

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"
	"time"

	"0xacab.org/leap/ooni-exporter/ooniexporter"
	"0xacab.org/leap/ooni-exporter/ooniexporter/model"
	"0xacab.org/leap/ooni-exporter/ooniexporter/testutils"
)

func TestFetchMeasurementSuccess(t *testing.T) {
	r := ioutil.NopCloser(readMeasurement(t, "./testdata/result_de_success.json"))
	client := testutils.NewMockClient()
	client.MockDo = func(*http.Request) (*http.Response, error) {
		fmt.Println(fmt.Sprint(client.EndpointMap))
		return &http.Response{
			StatusCode: 200,
			Body:       r,
		}, nil
	}

	measurement, err := ooniexporter.FetchMeasurement(client, newResult())
	if err != nil {
		if err.Err != nil {
			t.Fatal("unexpected error. expected: nil - actual: " + fmt.Sprint(err.Err))
		} else if err.HTTPStatus != nil {
			t.Fatal("unexpected HTTP error status code. expected: nil - actual: " + fmt.Sprint(err.HTTPStatus))
		} else {
			t.Fatal("unexpected error. expected: nil - actual: " + fmt.Sprint(err))
		}
	}

	if measurement == nil {
		t.Fatal("unexpected nil value for measurement struct")
	}
}

func TestFetchMeasurementSuccess_v030(t *testing.T) {
	r := ioutil.NopCloser(readMeasurement(t, "./testdata/result_de_success_v030.json"))
	client := testutils.NewMockClient()
	client.MockDo = func(*http.Request) (*http.Response, error) {
		fmt.Println(fmt.Sprint(client.EndpointMap))
		return &http.Response{
			StatusCode: 200,
			Body:       r,
		}, nil
	}

	measurement, err := ooniexporter.FetchMeasurement(client, newResult())
	if err != nil {
		if err.Err != nil {
			t.Fatal("unexpected error. expected: nil - actual: " + fmt.Sprint(err.Err))
		} else if err.HTTPStatus != nil {
			t.Fatal("unexpected HTTP error status code. expected: nil - actual: " + fmt.Sprint(err.HTTPStatus))
		} else {
			t.Fatal("unexpected error. expected: nil - actual: " + fmt.Sprint(err))
		}
	}

	if measurement == nil {
		t.Fatal("unexpected nil value for measurement struct")
	}
}

func TestFetchMeasurementHTTPCodeErrors(t *testing.T) {
	var testCases = [...]int{
		300, 301, 302, 303, 304, 305, 306, 307, 308,
		400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 429,
		500, 501, 502, 503, 504, 505,
	}

	for _, statusCode := range testCases {
		runFetchMeasurementHttpError(t, statusCode)
	}
}

func TestFetchMeasurementParsingErrors(t *testing.T) {

	r := ioutil.NopCloser(bytes.NewReader([]byte("<not a valid json>")))
	client := testutils.NewMockClient()
	client.MockDo = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: 200,
			Body:       r,
		}, nil
	}

	_, err := ooniexporter.FetchMeasurement(client, newResult())

	t.Log("error: " + fmt.Sprint(err))
	if err == nil {
		t.Fatal("unexpected nil error.")
	}
	if err.Err == nil {
		t.Fatal("unexpected nil error payload.")
	}

	if err.HTTPStatus != nil {
		t.Fatal("unexpected HTTP Status error code")
	}
}

func TestFetchSummarySuccess(t *testing.T) {
	r := ioutil.NopCloser(readMeasurement(t, "./testdata/measurements.json"))
	startTime := time.Now().AddDate(0, 0, -1)
	startTime = time.Date(startTime.Year(), startTime.Month(), startTime.Day(), 0, 0, 0, 0, startTime.Location())
	endTime := time.Now()
	endTime = time.Date(endTime.Year(), endTime.Month(), endTime.Day(), 0, 0, 0, 0, endTime.Location())

	client := testutils.NewMockClient()
	client.MockDo = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: 200,
			Body:       r,
		}, nil
	}

	summary, err := ooniexporter.FetchSummary(client, startTime, endTime)

	if err != nil {
		if err.Err != nil {
			t.Fatal("unexpected error payload " + fmt.Sprint(err.Err))
		} else if err.HTTPStatus != nil {
			t.Fatal("unexpected HTTP error status code: " + fmt.Sprint(err.HTTPStatus))
		} else {
			t.Fatal("unexpected non-nill error struct")
		}
	}

	if summary == nil {
		t.Fatal("unexpected nil value for summary struct")
	}

}

func TestFetchSummaryHTTPErrorCodes(t *testing.T) {
	var testCases = [...]int{
		300, 301, 302, 303, 304, 305, 306, 307, 308,
		400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 429,
		500, 501, 502, 503, 504, 505,
	}

	for _, statusCode := range testCases {
		runFetchSummaryHttpError(t, statusCode)
	}
}

func TestFetchSummaryParsingError(t *testing.T) {
	r := ioutil.NopCloser(bytes.NewReader([]byte("<not a valid json>")))
	startTime := time.Now().AddDate(0, 0, -1)
	startTime = time.Date(startTime.Year(), startTime.Month(), startTime.Day(), 0, 0, 0, 0, startTime.Location())
	endTime := time.Now()
	endTime = time.Date(endTime.Year(), endTime.Month(), endTime.Day(), 0, 0, 0, 0, endTime.Location())

	client := testutils.NewMockClient()
	client.MockDo = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: 200,
			Body:       r,
		}, nil
	}

	summary, err := ooniexporter.FetchSummary(client, startTime, endTime)

	t.Log("error: " + fmt.Sprint(err))
	if err == nil {
		t.Fatal("unexpected nil error.")
	}
	if err.Err == nil {
		t.Fatal("unexpected nil error payload.")
	}

	if err.HTTPStatus != nil {
		t.Fatal("unexpected HTTP Status error code")
	}

	if summary != nil {
		t.Fatal("unexpected non-null summary struct")
	}
}

func TestFetchDataFromAPISuccess(t *testing.T) {
	startTime := time.Now().AddDate(0, 0, -1)
	startTime = time.Date(startTime.Year(), startTime.Month(), startTime.Day(), 0, 0, 0, 0, startTime.Location())
	endTime := time.Now()
	endTime = time.Date(endTime.Year(), endTime.Month(), endTime.Day(), 0, 0, 0, 0, endTime.Location())

	ooniexporter.Client = initOONIExporterClient(t, 200, []int{200})
	resultData, errorResult := ooniexporter.FetchDataFromAPI(startTime, endTime, 1000)

	if errorResult == nil {
		t.Fatal("unexpected nil value for errorResult")
	}
	if errorResult.HasErrors() {
		t.Fatal("unexpected errors in errorResult: " + fmt.Sprint(errorResult))
	}
	if resultData == nil {
		t.Fatal("unexpected nil value for resultData")
	}
	if resultData.GetTestResultCount() != 4 {
		t.Fatal("unexpected amount of registered tests")
	}
	if len(resultData.FailingTestsByCountry()) != 1 {
		t.Fatal("unexpected amount of entries in FailingTestsByCountry: " + fmt.Sprint(resultData.FailingTestsByCountry()))
	}
	if resultData.FailingTestsByCountry()["DE"] != 0 {
		t.Fatal("unexpected amount of failing tests for FailingTestsByCountry()[\"DE\"]: " + fmt.Sprint(resultData.FailingTestsByCountry()["DE"]))
	}
	if len(resultData.FailingTestsByNetwork()) != 1 {
		t.Fatal("unexpected amount of entries in FailingTestsByNetwork(): " + fmt.Sprint(resultData.FailingTestsByNetwork()))
	}
	if resultData.FailingTestsByNetwork()["AS3209"] != 0 {
		t.Fatal("unexpected amount of failing tests for FailingTestsByCountry()[\"AS3209\"]: " + fmt.Sprint(resultData.FailingTestsByNetwork()["AS3209"]))
	}
	if len(resultData.SuccessfulTestsByCountry()) != 1 {
		t.Fatal("unexpected amount of entries in SuccessfulTestsByCountry(): " + fmt.Sprint(resultData.SuccessfulTestsByCountry()))
	}
	if resultData.SuccessfulTestsByCountry()["DE"] != 4 {
		t.Fatal("unexpected amount of test results in SuccessfulTestsByCountry()[\"DE\"]: " + fmt.Sprint(resultData.SuccessfulTestsByCountry()["DE"]))
	}
	if len(resultData.SuccessfulTestsByNetwork()) != 1 {
		t.Fatal("unexpected amount of entries in SuccessfulTestsByNetwork(): " + fmt.Sprint(resultData.SuccessfulTestsByNetwork()))
	}
	if resultData.SuccessfulTestsByNetwork()["AS3209"] != 4 {
		t.Fatal("unexpected amount of test results in SuccessfulTestsByNetwork()[\"AS3209\"]: " + fmt.Sprint(resultData.SuccessfulTestsByNetwork()["AS3209"]))
	}

}

func TestFetchDataFromAPIFailingSummary(t *testing.T) {
	startTime := time.Now().AddDate(0, 0, -1)
	startTime = time.Date(startTime.Year(), startTime.Month(), startTime.Day(), 0, 0, 0, 0, startTime.Location())
	endTime := time.Now()
	endTime = time.Date(endTime.Year(), endTime.Month(), endTime.Day(), 0, 0, 0, 0, endTime.Location())
	ooniexporter.Client = initOONIExporterClient(t, 404, []int{200})
	resultData, errorResult := ooniexporter.FetchDataFromAPI(startTime, endTime, 1000)

	if errorResult == nil {
		t.Fatal("unexpected missing errorResult")
	}
	if !errorResult.HasErrors() {
		t.Fatal("unexpected missing errors in errorResult")
	}
	if errorResult.GetAllErrors() != 0 {
		t.Fatal("unexpected number of collected errors: " + fmt.Sprint(errorResult.GetAllErrors()))
	}
	if errorResult.GetAllHTTPStatusCodes() != 1 {
		t.Fatal("unexpected number of collected HTTP error status codes: " + fmt.Sprint(errorResult.GetAllHTTPStatusCodes()))
	}

	if resultData != nil {
		t.Fatal("unexpected value for resultData")
	}
}

func TestFetchDataFromAPIFailingMeasurements(t *testing.T) {
	startTime := time.Now().AddDate(0, 0, -1)
	startTime = time.Date(startTime.Year(), startTime.Month(), startTime.Day(), 0, 0, 0, 0, startTime.Location())
	endTime := time.Now()
	endTime = time.Date(endTime.Year(), endTime.Month(), endTime.Day(), 0, 0, 0, 0, endTime.Location())
	ooniexporter.Client = initOONIExporterClient(t, 200, []int{404})
	resultData, errorResult := ooniexporter.FetchDataFromAPI(startTime, endTime, 1000)

	if errorResult == nil {
		t.Fatal("unexpected missing errorResult")
	}
	if !errorResult.HasErrors() {
		t.Fatal("unexpected missing errors in errorResult")
	}
	if errorResult.GetAllErrors() != 0 {
		t.Fatal("unexpected number of collected errors: " + fmt.Sprint(errorResult.GetAllErrors()))
	}
	if errorResult.GetAllHTTPStatusCodes() != 4 {
		t.Fatal("unexpected number of collected HTTP error status codes: " + fmt.Sprint(errorResult.GetAllHTTPStatusCodes()))
	}
	if resultData == nil {
		t.Fatal("resultData should never be null")
	}
	if resultData.GetTestResultCount() != 0 {
		t.Fatal("unexpected amount of valid test results")
	}
}

func initOONIExporterClient(t *testing.T, summaryStatusCode int, measurementStatusCode []int) ooniexporter.HTTPClient {
	client := testutils.NewMockClient()
	client.MockDo = func(request *http.Request) (*http.Response, error) {
		if strings.Contains(request.URL.Path, "/api/v1/measurements") {
			r := ioutil.NopCloser(readMeasurement(t, "./testdata/measurements.json"))
			return &http.Response{
				StatusCode: summaryStatusCode,
				Body:       r,
			}, nil
		} else if strings.Contains(request.URL.Path, "api/v1/raw_measurement") {
			i := client.EndpointMap["api/v1/raw_measurement"]
			size := len(measurementStatusCode)
			statusCode := measurementStatusCode[i%size]
			client.EndpointMap["api/v1/raw_measurement"] = i + 1
			r := getReadCloserForStatusCode(t, statusCode)
			return &http.Response{
				StatusCode: statusCode,
				Body:       r,
			}, nil
		} else {
			t.Fatal("unexpected API enpoint called: " + fmt.Sprint(request.URL.Path))
		}
		return nil, nil
	}
	return client
}

func TestFetchDataFromAPIPartialFailingMeasurements(t *testing.T) {
	startTime := time.Now().AddDate(0, 0, -1)
	startTime = time.Date(startTime.Year(), startTime.Month(), startTime.Day(), 0, 0, 0, 0, startTime.Location())
	endTime := time.Now()
	endTime = time.Date(endTime.Year(), endTime.Month(), endTime.Day(), 0, 0, 0, 0, endTime.Location())
	ooniexporter.Client = initOONIExporterClient(t, 200, []int{200, 404, 404, 200})
	resultData, errorResult := ooniexporter.FetchDataFromAPI(startTime, endTime, 1000)

	if errorResult == nil {
		t.Fatal("unexpected missing errorResult")
	}
	if !errorResult.HasErrors() {
		t.Fatal("unexpected missing errors in errorResult")
	}
	if errorResult.GetAllErrors() != 0 {
		t.Fatal("unexpected number of collected errors: " + fmt.Sprint(errorResult.GetAllErrors()))
	}
	if errorResult.GetAllHTTPStatusCodes() != 2 {
		t.Fatal("unexpected number of collected HTTP error status codes: " + fmt.Sprint(errorResult.GetAllHTTPStatusCodes()))
	}
	if resultData == nil {
		t.Fatal("resultData should never be null")
	}
	if resultData.GetTestResultCount() != 2 {
		t.Fatal("unexpected amount of valid test results")
	}
}

func getSummaryReadCloserForStatusCode(t *testing.T, statusCode int) io.ReadCloser {
	if statusCode >= 400 {
		return nil
	}
	return ioutil.NopCloser(readMeasurement(t, "./testdata/measurements.json"))
}

func getReadCloserForStatusCode(t *testing.T, statusCode int) io.ReadCloser {
	if statusCode >= 400 {
		return nil
	}
	return ioutil.NopCloser(readMeasurement(t, "./testdata/result_de_success.json"))
}

func runFetchMeasurementHttpError(t *testing.T, statusCode int) {
	r := getReadCloserForStatusCode(t, statusCode)
	client := testutils.NewMockClient()
	client.MockDo = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: statusCode,
			Body:       r,
		}, nil
	}

	measurement, err := ooniexporter.FetchMeasurement(client, newResult())
	if err == nil {
		t.Fatal("unexpected nil error.")
	}
	if err.Err != nil {
		t.Fatal("unexpected error. expected: nil - actual: " + fmt.Sprint(err.Err))
	}

	if err.HTTPStatus == nil {
		t.Fatal("unexpected nil value for HTTPStatus")
	}
	if *err.HTTPStatus != statusCode {
		t.Fatal("unexpected HTTP error status code. expected: " + fmt.Sprint(statusCode) + " - actual: " + fmt.Sprint(err.HTTPStatus))
	}

	if measurement != nil {
		t.Fatal("unexpected non-null measurement struct")
	}
}

func runFetchSummaryHttpError(t *testing.T, statusCode int) {
	startTime := time.Now().AddDate(0, 0, -1)
	startTime = time.Date(startTime.Year(), startTime.Month(), startTime.Day(), 0, 0, 0, 0, startTime.Location())
	endTime := time.Now()
	endTime = time.Date(endTime.Year(), endTime.Month(), endTime.Day(), 0, 0, 0, 0, endTime.Location())
	r := getSummaryReadCloserForStatusCode(t, statusCode)
	client := testutils.NewMockClient()
	client.MockDo = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: statusCode,
			Body:       r,
		}, nil
	}

	summary, err := ooniexporter.FetchSummary(client, startTime, endTime)

	if err == nil {
		t.Fatal("unexpected nil error.")
	}
	if err.Err != nil {
		t.Fatal("unexpected error. expected: nil - actual: " + fmt.Sprint(err.Err))
	}

	if err.HTTPStatus == nil {
		t.Fatal("unexpected nil value for HTTPStatus")
	}
	if *err.HTTPStatus != statusCode {
		t.Fatal("unexpected HTTP error status code. expected: " + fmt.Sprint(statusCode) + " - actual: " + fmt.Sprint(err.HTTPStatus))
	}

	if summary != nil {
		t.Fatal("unexpected non-null summary struct")
	}
}

func readMeasurement(t *testing.T, path string) *bytes.Reader {
	testfile, err := ioutil.ReadFile(path)
	if err != nil {
		err = errors.New("getMeasurement could not open test file: " + fmt.Sprint(err))
		t.Fatal(err)
	}

	return bytes.NewReader(testfile)
}

func newResult() model.Result {
	result := new(model.Result)
	result.Anomaly = false
	result.Confirmed = false
	result.Failure = false
	result.MeasurementStartTime = ""
	result.MeasurementURL = ""
	result.ProbeASN = ""
	result.ProbeCC = ""
	return *result
}
